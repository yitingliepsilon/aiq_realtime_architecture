# README #

* The elasticdump lambda is  part of the ETL process to generate batch features for real-time scoring purpose.

### 7 steps invovled ###

* get environment variable
* get event variable
* get last_success_timestamp from s3
* get tenant settings from s3
* get pem secret string from secret manager
* establish ssh connection to exsting ec2 server.
* construct the ealsticdump query.
* execute the query.

### How to deploy ###

* execute './deploy.sh {env}'
* env could be dev,qa,prod etc.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact