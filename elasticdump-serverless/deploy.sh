#!/bin/bash
python3 ./generate_env.py
cd app
serverless deploy --force --stage $1
rm ../env.json