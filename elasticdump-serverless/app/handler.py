#!/usr/bin/env python
# coding: utf-8




import boto3
import json
import paramiko
import os
import time
import base64
from datetime import datetime
from ElasticDump import ElasticDump
from ssh import SSH
import io
import logging

logger = logging.getLogger(__name__)

#todo -loglevel
logger.setLevel(logging.DEBUG)

# Simple exception wrappers
class ClientException(Exception):
    pass

class LambdaException(Exception):
    pass

def handler(event, context):
    
    s3_client = boto3.client('s3')
    ssm_client = boto3.client('secretsmanager')
    point_in_time = int(round(time.time() * 1000))
    date = datetime.now().strftime("%m-%d-%Y")
    '''
    main handler of lambda function.
    7 steps involved:
      0) get environment variable
      1) get event variable
      2) get last_success_timestamp from s3
      3) get tenant settings from s3
      4) get pem secret string from secret manager
      5) establish ssh connection to exsting ec2 server.
      6) construct the ealsticdump query.
      7) execute the query.
    '''
    try:
        #get environment variable
        elasticDumpEc2PemSecretId = os.environ['EC2_PEMID']
        ec2Host = os.environ['EC2_HOST']
        env = os.environ['env']

        #get event variable
        event_keys = event.keys()
        try:
            if 'tenant' not in event_keys:
                msg = "missing field [tenant]"
                raise ClientException(msg)
        except ClientException as e:          
            logger.error(e, exc_info=True)
            return {"error": 400 , "message": msg}
        else:
            try:
                tenantID = event['tenant']
                if not isinstance(tenantID, str):
                    msg = "[tenant] not string"
                    raise ClientException(msg)
            except ClientException:
                logger.error(e, exc_info=True)
                return {"error": 400 , "message": msg}
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error getting event or environment variables,check log for details"}
    
    #get last_success_timestamp from s3
    try:
        def getlastSuccessTimestamp(env = None, s3_client = None, tenantID = None):
            bucket = 'aiqt-data'
            key = tenantID + '/EVENTS_ELASTICSEARCH/INCOMING/last_success_timestamp'
            last_success_timestamp = s3_client.get_object(Bucket = bucket ,Key = key)
            last_success_timestamp = last_success_timestamp['Body'].read().decode('utf-8')
            last_success_timestamp = json.loads(last_success_timestamp)
            return last_success_timestamp
        start_time = getlastSuccessTimestamp(s3_client = s3_client, tenantID = tenantID)
        start_time = str(start_time)
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 404 , "message": "error getting last_success_timestamp from s3,probably key not found, check log for details."}
    #get tenant setting from s3 bucket
    try:
        def getTenantSettings(env = None, tenantID = None, s3_client = None):
            bucket = 'iq-data-' + env
            key = tenantID + '/__SETTINGS__/'+ tenantID + '-' + env + '-tenant-settings.json'
            tenantSettings = s3_client.get_object(Bucket = bucket ,Key = key)
            tenantSettings = tenantSettings['Body'].read().decode('utf-8')
            tenantSettings = json.loads(tenantSettings)
            return tenantSettings

        tenantSettings = getTenantSettings(env = env, tenantID = tenantID, s3_client = s3_client)
        es_usernameid = tenantSettings["integrations"]["events"]["elkUsernameSecretId"]
        es_passwordid = tenantSettings["integrations"]["events"]["elkPasswordSecretId"] 
        #using the 1a es cluster
        es_endpoint = tenantSettings["integrations"]["events"]['elkEndpoints'].split(',')[0]
        es_index = tenantSettings["integrations"]["events"]['elkIndex']
        parentID = tenantSettings["integrations"]["events"]['parentId']
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error getting tenant setting, check log for details"}
    #get secret pem string and elk credentiasl from secret managers
    try:
        es_username = ssm_client.get_secret_value(SecretId = es_usernameid)['SecretString']
        es_password = ssm_client.get_secret_value(SecretId = es_passwordid)['SecretString']
        response = ssm_client.get_secret_value(SecretId = elasticDumpEc2PemSecretId)
        secret_string  = base64.b64decode(response['SecretString']).decode('utf-8')
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error getting strings from secret manager, check log for details"}

        #construct command script
    try:
        output_path = "s3://aiqt-data/{}/EVENTS_ELASTICSEARCH/INCOMING/".format(tenantID)
        elasticDumpLogLocation = "/home/ec2-user/elasticdump_logs/{}/elasticDumpLogs/".format(tenantID)
        kwargs = {} 
        ed_command_script,ed_command_script_log = ElasticDump.constructEDquery(s3_region = 'us-east-1',
                                                                               es_username = es_username,
                                                                               es_password = es_password,
                                                                               es_endpoint = es_endpoint,
                                                                               es_index = es_index,
                                                                               parentID = parentID,
                                                                               output_path = output_path,
                                                                               elasticDumpLogLocation = elasticDumpLogLocation,
                                                                               point_in_time = point_in_time,
                                                                               start_time = start_time,
                                                                               date = date,
                                                                               **kwargs)
        print(ed_command_script_log)
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error building the execution command, check log for details"}
    #command list to execute one by one
    try:        
        #ssh_instance.execute(ed_command_script)
        
        #construct the tmp file in interactive-shell mode
        ssh_instance = SSH(address = ec2Host, username = 'ec2-user', secret_string = secret_string)
        ssh_instance.connnectInteractive()
        ssh_instance.openShell()
        command_factory = ElasticDump.constructCommandFactory(ed_command_script = ed_command_script,
                                                              tenantID = tenantID, 
                                                              point_in_time = point_in_time,
                                                              date = date)
        for command in command_factory:
            ssh_instance.sendShell(command)
            time.sleep(0.1)           
        ssh_instance.closeConnection()
        
        #execute the tmp file in noninteractive  mode
        ssh_instance = SSH(address = ec2Host, username = 'ec2-user', secret_string = secret_string)
        ssh_instance.connnectNoninteractive()
        command = "nohup /home/ec2-user/{}-tmp.sh &".format(tenantID)
        ssh_instance.execute(command)
        time.sleep(1)
        ssh_instance.closeConnection()
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error establishing ssh connection to ec2 or executing the command, check log for details"}

    return { 'code': 200, 'message' : 'Script execution started. Check logs for details'}