#!/usr/bin/env python
# coding: utf-8
# this script is being used to generate env.json file

import json
import boto3

#save my_dict to env.json in current directory
def writeJsonFile(pythonDict):
    with open('env.json', 'w') as fp:
        json.dump(pythonDict, fp)
        
client = boto3.client('s3')
clientSSM= boto3.client('secretsmanager')

#download iq env config:
bucket = 'iq-client-data-dev'
key = '__CONFIG__/dev-settings.json'
iqConfig = client.get_object(Bucket = bucket ,Key = key)
iqConfig = iqConfig['Body'].read().decode('utf-8')
iqConfig = json.loads(iqConfig)

#download tenant setting:
key = 'BJ/__SETTINGS__/BJ-settings.json'
tenantSetting = client.get_object(Bucket = bucket ,Key = key)
tenantSetting = tenantSetting['Body'].read().decode('utf-8')
tenantSetting = json.loads(tenantSetting)

#get the elk username and password from aws secured secret manager
for i in ["eventsElkUsernameSecretId","eventsElkPasswordSecretId"]:
    response = clientSSM.get_secret_value(SecretId = tenantSetting[i])
    tenantSetting[i.replace('Id','String')] = response['SecretString']
  
#download model parameter:(pending discussion with modeling team)
modelParam = {}
#modelParam['modelID'] = 'xgboost'
#modelParam['modelVersion'] = 'v1'
modelParam['lookBackInterval'] = '24h'
modelParam['eventCodesDict'] = "{'AbandonBrowse': '1', 'AbandonCart': '2', 'Conversion': '3', 'ProductBrowse': '4'}"
modelParam['sagemakerEndpoint'] = 'custom-combined-ep-bjs-2020-05-18-21-34-12'

#combine the 3 dictionaries
envDict = {**iqConfig, **tenantSetting, **modelParam}
writeJsonFile(envDict)
