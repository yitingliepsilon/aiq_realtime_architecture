#import necessary packages
import boto3
import pandas as pd
from rediscluster import StrictRedisCluster
import s3fs
import time

#define metadata
bucket = 'aiqt-data'
path = 'BJ/events/events-mapping/mapping_table.csv/' #if its a directory omit the t$
access_key = 'AKIAV23ZL53A26SRLMUW'
secret_key = '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'
host = 'profiles-cluster.gorowf.clustercfg.use1.cache.amazonaws.com'
port = '6379'

#establish s3 and elasticache connection
startup_nodes = [{"host": host, "port": port}]
r = StrictRedisCluster(startup_nodes=startup_nodes, decode_responses=True, skip_full_coverage_check=True)
s3_client = boto3.client(
    's3',
    # Hard coded strings as credentials, not recommended.
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key
)

#locate and download the mapping table from s3
response = s3_client.list_objects(
    Bucket= bucket,
    Prefix= path
)
for file in response['Contents']:
    if 'part-00' in file['Key']:
        file_name = file['Key']        
obj = s3_client.get_object(Bucket=bucket, Key=file_name)
df = pd.read_csv(obj['Body'],names=['key','value'], header=None).to_numpy()        

#Insert mapping table into elasticache
print("start inserting mapping_table to " + host+":"+port)
pipe = r.pipeline()
start_time = time.time()
count = 0
for row in df:
    count+=1
    pipe.set(row[0], row[1])
    if int(count)%10000 == 0 :
        results = pipe.execute()
        print(str(count)+" rows upserted!")
        print("--- %s seconds ---" % (time.time() - start_time))
        pipe = r.pipeline()
print("All done with %s seconds. Program ended! " % (time.time() - start_time))
