#!/usr/bin/env python
# coding: utf-8

# In[3]:


import boto3
import pandas as pd
import numpy as np
import redis
from rediscluster import  StrictRedisCluster
import pyarrow
import pyarrow.parquet
import s3fs
import time


# In[25]:


bucket = 'aiqt-data'
path = 'BJ/events/events-raw/2020-04-09.parquet' #if its a directory omit the traling /
access_key = 'AKIAV23ZL53A26SRLMUW'
secret_key = '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'
host = 'profiles-cluster.gorowf.clustercfg.use1.cache.amazonaws.com'
port = '6379'


# In[26]:


#read data from s3 parquet file as pandas dataframe
#probably need to do this in batch??
fs = s3fs.S3FileSystem(anon=False,
                            key=access_key, 
                            secret=secret_key)

bucket_uri = 's3://'+bucket+'/'+path

dataset = pyarrow.parquet.ParquetDataset(bucket_uri, filesystem=fs)
print("file successfully read from" + bucket_uri)
table = dataset.read()
print("converting to pandas dataframe")
df = table.to_pandas()
#future optimized  work on pyarrow table directly.


# In[40]:


#prepare dataframe to be ready to load into redis
print("preparing dataframe...")
df['index'] = df.index
df = df[['index']+list(set(df.columns)-set(['index']))]
df_numpy = df.to_numpy().astype(str)


# In[5]:


startup_nodes = [{"host": host, "port": port}]
r = StrictRedisCluster(startup_nodes=startup_nodes, decode_responses=True, skip_full_coverage_check=True)


# In[39]:


'''pipe = r.pipeline()
start_time = time.time()
for row in df.itertuples():
    #key = str(index)
    print(type(row))
    value = row.to_json()
    #value = 'test'
    #value = ' '.join([str(i) for i in row[list(set(df.columns)-set(['index']))].values])
    pipe.set(key, value)
    if int(key)%10000 == 0 :
        results = pipe.execute()
        print(key+" done!")
        print("--- %s seconds ---" % (time.time() - start_time))
        #pipe = r.pipeline()
'''


# In[42]:


#try numpy!!!
print("start inserting features to " + host+":"+port)
pipe = r.pipeline()
start_time = time.time()
for key,value in zip(df_numpy[:,0], df_numpy[:,1:]):
    pipe.set(key, value.tobytes())
    if int(key)%10000 == 0 :
        results = pipe.execute()
        print(str(key)+" rows upserted!")
        print("--- %s seconds ---" % (time.time() - start_time))
        pipe = r.pipeline()
print("All done with %s seconds. Program ended! " % (time.time() - start_time))


# In[41]:


#np.frombuffer((r.get('0')).encode("utf-8"),dtype = '<U36')
#r.flushdb()


# In[4]:

