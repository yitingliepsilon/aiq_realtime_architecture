# README #

This is the subrepo of orchestration repo. It contains some lambda code and shell scripts related real-time scoring. It will further merged to orchestration repo later.

### The repo contains the following components:  ###
* recommendation function lambda
* elasticdump lambda
* scripts to setup ec2 instance
* notebooks to write env and tenant settings to s3 bucket
* notebooks to write env and tenant settings to elasticache

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact