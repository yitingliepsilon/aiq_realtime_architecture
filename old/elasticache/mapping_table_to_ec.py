#!/usr/bin/env python
# coding: utf-8

# In[2]:


#import necessary packages
import pandas as pd
from rediscluster import StrictRedisCluster
import time
import pyarrow.parquet as pq
import s3fs


# In[3]:


def file_to_numpy(s3,BU):
    numpy_array = pq.ParquetDataset('s3://'+ BU['env']['dev']['bucket'] + '/' + BU['env']['dev']['file_name'], filesystem=s3).read_pandas().to_pandas().to_numpy() 
    return numpy_array


# In[10]:


def batch_insert(np_array,BU,redis):
    print("start inserting mapping_table to " + BU['env']['dev']['host'] + ":" + BU['env']['dev']['port'])
    pipe = redis.pipeline()
    start_time = time.time()
    count = 0
    for row in np_array:
        count+=1
        row[0] =  BU['name'] + ':' + BU['env']['dev']['model']['model_name'] + ':' + BU['env']['dev']['model']['model_version'] + ':model:prod_cat_mapping:' + row[0]
        pipe.set(row[0], row[1])
        if int(count)%10000 == 0 :
            results = pipe.execute()
            print(str(count)+" rows upserted!")
            print("--- %s seconds ---" % (time.time() - start_time))
            pipe = r.pipeline()
    pipe.execute()
    print("All done with %s seconds. Program ended! " % (time.time() - start_time))


# In[11]:


BU = {}
BU['name'] = '60494'
BU['env'] = {}
BU['env']['dev'] = {}
BU['env']['dev']['bucket'] = 'aiqt-data'
BU['env']['dev']['file_name'] = 'BJ/RAW_DATA/Prod_Cat_SKU.parquet' #if its a directory omit the t$
BU['env']['dev']['host'] = 'profiles-cluster.gorowf.clustercfg.use1.cache.amazonaws.com'
BU['env']['dev']['port'] = '6379'
BU['env']['dev']['model'] = {}
BU['env']['dev']['model']['model_name'] = 'xgboost'
BU['env']['dev']['model']['model_version'] = '1_0'


# In[12]:


#redis and s3 connection
startup_nodes = [{"host": BU['env']['dev']['host'], "port": BU['env']['dev']['port']}]
r = StrictRedisCluster(startup_nodes=startup_nodes, decode_responses=True, skip_full_coverage_check=True)
s3 = s3fs.S3FileSystem()


# In[13]:


np_array = file_to_numpy(s3,BU)


# In[ ]:


batch_insert(np_array,BU,r)


# In[ ]:




