from ast import literal_eval
import json

class EC():
    def __init__(self):
        pass
    
    @staticmethod
    def getBatchIVs(redis = None,
                    tenantID = None,
                    applicationID = None,
                    modelID = None,
                    modelRunID = None,
                    customerID = None):
        
        key = tenantID + ':' + applicationID + ':' + modelID + ':' + modelRunID + ':iv:'+ customerID
        try:
            result = redis.get(key)
        except:
            raise

        if result is not None:
            #IVs = np.frombuffer(result.encode("utf-8"),dtype = '<U36')
            #todo jsonload
             IVs = list(literal_eval(result))
        else:
            IVs = None
        return IVs
    
    @staticmethod
    def getMapping(redis = None,
                   df_mapped = None,
                   tenantID = None,
                   applicationID = None,
                   modelID = None,
                   modelRunID = None
                   ):
        list_PROD_CAT_LU = []
        for _, i in df_mapped.iterrows():
            key = tenantID + ':' + applicationID + ':' + modelID + ':' + modelRunID + ':model:prod_cat_mapping:' + i['objectType'] + '_' + i['objectId']
            #logging
            try:
                 value = redis.get(key)
            except:
                raise
            list_PROD_CAT_LU.append(value)
        df_mapped['PROD_CAT_LU'] = list_PROD_CAT_LU
        df_mapped = df_mapped[['event_CD','PROD_CAT_LU','eventTimestamp']]
        #self.EventsDFPandas['eventTimeStamp'] = self.EventsDFPandas['eventTimestamp'].astype(int)
        df_mapped = df_mapped[df_mapped['PROD_CAT_LU'].notnull()]
        return df_mapped
    
    @staticmethod
    def getOrderIVInfo(redis = None,
                       tenantID = None,
                       applicationID = None,
                       modelID = None,
                       modelRunID = None):
        key = tenantID + ':' + applicationID + ':' + modelID + ':' + modelRunID + ':model_mapping'
        try:
            result = redis.get(key)
        except:
            raise
            
        #todo logging    
        if result is not None:
            #IVs = np.frombuffer(result.encode("utf-8"),dtype = '<U36')
            #todo json load 
            result = list(literal_eval(result))
        else:
            result = None            
        return result
    
    @staticmethod
    def getOrderIVBatch(redis = None,
                        tenantID = None,
                        applicationID = None,
                        modelID = None,
                        modelRunID = None):
        key = tenantID + ':' + applicationID + ':' + modelID + ':' + modelRunID + ':ivname'
        try:
            result = redis.get(key)
        except:
            raise
        #todo logging     
        if result is not None:
            #IVs = np.frombuffer(result.encode("utf-8"),dtype = '<U36')
            #todo json load
            result = list(literal_eval(result))
        else:
            result = None
            #todo logging
            print("batch iv name not found: {}".format(key))
        return result
    
    @staticmethod
    def getTenantSettings(redis, tenantID, env):
        key = tenantID + ':' + env + '-tenant-settings'
        try:
            response = redis.get(key)
        except:
            raise
        if not response:
            return response
        else:
            return json.loads(response)
    
    @staticmethod
    def getModelSettings(redis, tenantID, applicationID, modelID, modelRunID):
        key = tenantID + ':' + applicationID + ':' + modelID + ':' + modelRunID + ':model-settings'
        try:
            response = redis.get(key)
        except:
            raise
        if not response:
            return response
        else:
            return json.loads(response)
        