#!/usr/bin/env python
# coding: utf-8

# In[4]:


#import necessary packages
import pandas as pd
from rediscluster import StrictRedisCluster
import time
import boto3


# In[13]:


def File_to_string(s3_client,BU):
    result = s3_client.get_object(Bucket=BU['env']['dev']['bucket'], Key=BU['env']['dev']['orderIV_file_name']) 
    text = result["Body"].read().decode()
    return text


# In[15]:


def OrderIV_insert(OrderIV_string,BU,redis):
    key=  BU['name'] + ':' + BU['env']['dev']['model']['model_name'] + ':' + BU['env']['dev']['model']['model_version'] + ':model_mapping'
    value = OrderIV_string
    try:
        redis.set(key,value)
    except (redis.exceptions.ConnectionError, 
            redis.exceptions.BusyLoadingError):
        return False
    print("OrderIV upserted!")
    return True


# In[16]:


BU = {}
BU['name'] = '60494'
BU['env'] = {}
BU['env']['dev'] = {}
BU['env']['dev']['bucket'] = 'aiqt-model-dev'
BU['env']['dev']['orderIV_file_name'] = 'TZ/BJs/CS/Run042920/schema/schema.json' 
BU['env']['dev']['host'] = 'profiles-cluster.gorowf.clustercfg.use1.cache.amazonaws.com'
BU['env']['dev']['port'] = '6379'
BU['env']['dev']['model'] = {}
BU['env']['dev']['model']['model_name'] = 'xgboost'
BU['env']['dev']['model']['model_version'] = '1_0'


# In[17]:


#redis and s3 connection
startup_nodes = [{"host": BU['env']['dev']['host'], "port": BU['env']['dev']['port']}]
r = StrictRedisCluster(startup_nodes=startup_nodes, decode_responses=True, skip_full_coverage_check=True)
s3_client = boto3.client('s3')


# In[18]:


OrderIV_string = File_to_string(s3_client,BU)


# In[19]:


OrderIV_insert(OrderIV_string,BU,r)

