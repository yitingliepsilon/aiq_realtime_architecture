!#/bin/bash
sudo yum install epel-release
sudo yum update
sudo yum install git
sudo amazon-linux-extras install redis4.0
sudo yum install python3
sudo curl -O https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
pip install jupyter
pip install redis-py-cluster==1.3.6
pip install pandas
pip install boto3
pip install pyarrow
pip install s3fs
pip install nbconvert
pip install virtualenv
curl –sL https://rpm.nodesource.com/setup_10.x | sudo bash -
sudo yum install –y nodejs
npm install elasticdump -g
npm install -g serverless
conda install jupyterthemes
jt -t chesterish
mkdir elasticdump_logs

