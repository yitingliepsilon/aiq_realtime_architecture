import time
from datetime import datetime
class ElasticDump():    
    def __init__():
        return    
    @staticmethod
    def constructEDquery(s3_region = None,
                        es_username = None,
                        es_password = None,
                        es_endpoint = None,
                        es_index = None,
                        parentID = None,
                        output_path = None,
                        elasticDumpLogLocation = None,
                        point_in_time = None,
                        start_time = None,
                        date = None,
                        **kwargs
                        ):
        query = '',
        #todo  fix time window.
        end_time = str(point_in_time)
    
        #load in optional parameters.
        if 'LIMIT' in kwargs.keys():
            limit = kwargs['LIMIT']
        else:
            limit = str(10000)
        if 'CONCURRENCY' in kwargs.keys(): 
            concurrency = kwargs['CONCURRENCY']
        else:
            concurrency = str(3)
        if 'CONCURRENCYINTERVAL' in kwargs.keys():
            concurrencyInterval = kwargs['CONCURRENCYINTERVAL']
        else:
            concurrencyInterval = str(500)
        
        #construct command script
        script = 'elasticdump --s3Region="'
        script_log = script
        script += s3_region
        script_log += s3_region
        script += '''" --input='\\''https://'''
        script_log += '''" --input='\\''https://'''
        script += es_username
        script_log += 'es_username'
        script += ':'
        script_log += ':'
        script += es_password
        script_log += 'es_password'
        script += '@'
        script_log += '@'
        script += es_endpoint
        script_log += es_endpoint
        script += '/'
        script_log += '/'
        script += es_index
        script_log += es_index
        script += "/'\\'' --output="
        script_log += "/'\\'' --output="
        script += output_path
        script_log += output_path
        if not output_path.endswith('/'):
            script += '/'
            script_log += '/'
        script += date
        script_log += date
        script += '''.json --type=data --searchBody='\\''{"_source":["message"],"query":{"bool":{"filter":[{"term":{"log_group":"/aws/lambda/KinesisEventHandler"}},{"term":{"parentId": "'''
        script_log += '''.json --type=data --searchBody='\\''{"_source":["message"],"query":{"bool":{"filter":[{"term":{"log_group":"/aws/lambda/KinesisEventHandler"}},{"term":{"parentId": "'''
        script += parentID
        script_log += parentID
        script += '"}},{"term":{"loggerName":"com.epsilon.agilityevents.handler.BaseEventHandler"}},{"term":{"level":"INFO"}},{"range":{"@timestamp":{"gte":'
        script_log += '"}},{"term":{"loggerName":"com.epsilon.agilityevents.handler.BaseEventHandler"}},{"term":{"level":"INFO"}},{"range":{"@timestamp":{"gte":'
        script += start_time
        script_log += start_time
        script += ',"lt":'
        script_log += ',"lt":'
        script += end_time
        script_log += end_time
        script += ''',"format":"epoch_millis"}}}]}}}'\\'' --limit='''
        script_log += ''',"format":"epoch_millis"}}}]}}}'\\'' --limit='''
        script += limit
        script_log += limit
        script += ' --concurrency='
        script_log += ' --concurrency='
        script += concurrency
        script_log += concurrency
        script += ' --concurrencyInterval='
        script_log += ' --concurrencyInterval='
        script += concurrencyInterval
        script_log += concurrencyInterval
        script += ' --sourceOnly=true --noRefresh=true --support-big-int=true'
        script_log += ' --sourceOnly=true --noRefresh=true --support-big-int=true'
        #todo add BU folder
        script = script + ' >' + elasticDumpLogLocation + date + '.log 2>&1 '
        script_log = script_log + ' >' + elasticDumpLogLocation + date + '.log 2>&1 '
        #script = 'nohup '+ script + ' >' + elasticDumpLogLocation + date + '.log 2>&1 &'
        #script_log = 'nohup '+ script_log + ' >' + elasticDumpLogLocation + date + '.log 2>&1 &'
        return script, script_log
    
    @staticmethod
    def constructCommandFactory(ed_command_script = None,
                                tenantID = None, 
                                point_in_time = None,
                                date = None):
        command_factory = []
        command_factory.append("echo '#/bin/sh' > /home/ec2-user/{}-tmp.sh".format(tenantID))
        command_factory.append("chmod +x /home/ec2-user/{}-tmp.sh".format(tenantID))
        command_factory.append("echo '{0}' >> /home/ec2-user/{1}-tmp.sh".format(ed_command_script,tenantID))
        command_factory.append("echo 'if [ $? -eq 0 ]' >> /home/ec2-user/{}-tmp.sh".format(tenantID))
        command_factory.append("echo 'then' >> /home/ec2-user/{}-tmp.sh".format(tenantID))
        command_factory.append("echo '  mv /home/ec2-user/elasticdump_logs/{0}/elasticDumpLogs/{1}.log /home/ec2-user/elasticdump_logs/{0}/elasticDumpLogs/{1}-success.log' >> /home/ec2-user/{0}-tmp.sh".format(tenantID, date))
        command_factory.append("echo '  echo {0}  > /home/ec2-user/elasticdump_logs/{1}/last_success_timestamp' >> /home/ec2-user/{1}-tmp.sh".format(point_in_time, tenantID))
        command_factory.append("echo '  aws s3 cp /home/ec2-user/elasticdump_logs/{0}/last_success_timestamp s3://aiqt-data/{0}/EVENTS_ELASTICSEARCH/INCOMING/last_success_timestamp' >> /home/ec2-user/{0}-tmp.sh".format(tenantID))
        command_factory.append("echo '  aws sns publish --topic-arn arn:aws:sns:us-east-1:401297108673:elasticdump-fail --message \"elasticdump job sucessful for tenant {0}\"' >> /home/ec2-user/{0}-tmp.sh".format(tenantID))
        command_factory.append("echo 'else' >> /home/ec2-user/{}-tmp.sh".format(tenantID))
        command_factory.append("echo '  mv /home/ec2-user/elasticdump_logs/{0}/elasticDumpLogs/{1}.log /home/ec2-user/elasticdump_logs/{0}/elasticDumpLogs/{1}-error.log' >> /home/ec2-user/{0}-tmp.sh".format(tenantID, date))
        command_factory.append("echo '  aws sns publish --topic-arn arn:aws:sns:us-east-1:401297108673:elasticdump-fail --message \"elasticdump job failed for tenant {0}\"' >> /home/ec2-user/{0}-tmp.sh".format(tenantID))
        command_factory.append("echo 'fi' >> /home/ec2-user/{}-tmp.sh".format(tenantID))
        command_factory.append("echo 'rm $0' >> /home/ec2-user/{}-tmp.sh".format(tenantID))
        return command_factory