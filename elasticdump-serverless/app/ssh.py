import io
import paramiko

class SSH():
    shell = None
    client = None
    transport = None

    def __init__(self, address, username, secret_string):
        self.address = address
        print("Connecting to server on ip", str(address) + ".")
        pkey = paramiko.RSAKey.from_private_key(io.StringIO(secret_string))
        self.client = paramiko.client.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy())
        self.client.connect(address, username=username, pkey = pkey, timeout = 3)
        
    def connnectNoninteractive(self):
        self.transport = self.client.get_transport()
        self.channel = self.transport.open_session()
        print("Connected to server on ip", str(self.address) + ". non-interactive-mode")
    
    def execute(self,command):
        if (self.channel):
            self.channel.exec_command(command)
        else:
            self.channel = self.transport.open_session()
            self.channel.exec_command(command)
            
    def connnectInteractive(self):
        print("Connected to server on ip", str(self.address) + ". interactive-mode")

    def closeConnection(self):
        if(self.client != None):
            self.client.close()
            #self.transport.close()

    def openShell(self):
        self.shell = self.client.invoke_shell()

    def sendShell(self, command):
        if(self.shell):
            self.shell.send(command + "\n")
        else:
            print("Shell not opened.")
            raise Exception("Shell not opened.")