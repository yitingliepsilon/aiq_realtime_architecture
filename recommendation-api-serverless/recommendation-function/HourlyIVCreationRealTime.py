import re
import json

#logging
import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

class HourlyIVCreationRealTime():
    
    def __init__(self, ref_time = None):
        self.ref_time = ref_time
    
    def aggCount(self,matrixNP = None):
        return len(matrixNP)
    
    def aggR2L(self,matrixNP = None):
        if len(matrixNP) == 0:
            return None
        return (self.ref_time - max(matrixNP[:,2]))/1000
    
    def aggR2F(self,matrixNP = None):
        if len(matrixNP) == 0:
            return None
        return (self.ref_time - min(matrixNP[:,2]))/1000
    
        
    #transform function to generate real-time features from pandas df.
    def transform(self, OrderIV = None, df = None):
        result = []
        #numpy opreration to save time:
        matrixNP = df.values
        #todo logging
        logger.debug(matrixNP)
        for iv in OrderIV:
            #feature_type, agg_type = iv['name'].split('_')            
            #if feature_type[-1] != 'H':
            #   continue
            #todo logging
            if iv.split('_')[0][-1] != 'H':
                raise Exception("wrong batchfeatureivname: {} in [transform]".format(iv))
                continue
            feature_type, agg_type = iv.split('_')
            # 3 scenarios:
            # PTH,EPTH,ETH
            # EPTH
            if 'E' in feature_type and 'P' in feature_type:
                m = re.match("E(.*)P(.*)T(.*)H", feature_type)
                event_id = m.group(1)
                product_id = m.group(2)
                interval_id = m.group(3)
                cutoff_time = self.ref_time - int(interval_id)*3600*1000
                if product_id == 'A':
                    subset = matrixNP[(matrixNP[:,0] == event_id) & (matrixNP[:,2] >= cutoff_time)]
                else:
                    subset = matrixNP[(matrixNP[:,0] == event_id) & (matrixNP[:,1] == product_id) & (matrixNP[:,2] >= cutoff_time)]
            # ETH this could be duplicate features, could be removed in the near future
            elif 'E' in feature_type:
                m = re.match("E(.*)T(.*)H", feature_type)
                event_id = m.group(1)
                interval_id = m.group(2)
                cutoff_time = self.ref_time - int(interval_id)*3600*1000
                subset = matrixNP[(matrixNP[:,0] == event_id) & (matrixNP[:,2] >= cutoff_time)]
            elif 'P' in feature_type:
                m = re.match("P(.*)T(.*)H", feature_type)
                product_id = m.group(1)
                interval_id = m.group(2)
                cutoff_time = self.ref_time - int(interval_id)*3600*1000
                if product_id == 'A':
                    subset = matrixNP[matrixNP[:,2] >= cutoff_time]
                else:
                    subset = matrixNP[(matrixNP[:,1] == product_id) & (matrixNP[:,2] >= cutoff_time)]
            else:
                raise Exception ("feature_type not found on {} in [transform] ".format(iv))
            if agg_type == 'CNT':
                result.append((iv,self.aggCount(subset)))
            elif agg_type == 'R2L':
                result.append((iv,self.aggR2L(subset)))
            elif agg_type == 'R2F':
                result.append((iv,self.aggR2F(subset)))
            else:
                raise Exception("agg not found on {} in [transform]".format(iv))
        logger.debug(result)
        return result