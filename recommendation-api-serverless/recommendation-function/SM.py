import json

class SM():
    def __init__(self):
        pass
    
    @staticmethod
    def preparePayLoad(OrderIV = None,
                       OrderIV_batch = None,
                       batch_features = None,
                       realtime_features = None                   
                       ):
        feature_dict = {}
        result_str = ''
        for feature_name,feature_val in zip(OrderIV_batch, batch_features):
            if feature_val is None or feature_val == 'None':
                feature_val = '"NaN"'
            else:
                feature_val = '"' + str(feature_val) + '"'
            feature_dict[feature_name] = feature_val
        for feature_name,feature_val  in realtime_features:
            if feature_val is None or feature_val == 'None':
                feature_val = '"NaN"'
            else:
                feature_val = '"' + str(feature_val) + '"'
            feature_dict[feature_name] = feature_val  
        #for feature_name in OrderIV:
        #    feature_value = feature_dict[feature_name]
        #    if not isinstance(feature_value,str):
        #        feature_value = str(feature_value)
        #    feature_value_list.append(feature_value)
        #return ','.join(feature_value_list)
        feature_value_list = map(lambda x: feature_dict[x], OrderIV)
        payload = ','.join(feature_value_list)
        payload = '{{"data": [{}]}}'.format(payload)
        return payload
        # add error handling for feature_value with comma?
        #https://en.wikipedia.org/wiki/Comma-separated_values
        
    @staticmethod        
    def getPrediction(payload = None, runtime_client = None, sagemakerEndpoint = None ):        
        response = runtime_client.invoke_endpoint(EndpointName = sagemakerEndpoint, 
                                          ContentType = 'application/json', 
                                          Body = payload)
        raw_predictions = response['Body'].read().decode('ascii')
        return raw_predictions
    
    @staticmethod
    def standardizeFormat(prediction_string = None, num_of_choice = None):
        result = {}
        result['recommendations'] = []
        dv_dict = {}
        tokens = [i.replace('"','') for i in prediction_string.split(',')]
        index = 0
        while index < len(tokens):
            dv_dict[tokens[index]] = float(tokens[index+1])
            index +=2
        dv_sorted_list = sorted(dv_dict.items(), key = lambda x : x[1],reverse = True)
        if num_of_choice > len(dv_sorted_list) or num_of_choice < 0 :
            num_of_choice = len(dv_sorted_list)
        for i in range(num_of_choice):
            result['recommendations'].append({
                "id": dv_sorted_list[i][0],
                "score": dv_sorted_list[i][1],
                "rank": i+1
            })
        return result