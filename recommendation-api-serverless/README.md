# README #

The recommendation function lambda is in the middleware between the orchestration lambda and the sagemaker
* real-time model endpoint. It does the following tasks:

* Load real-time/hourly features from events elasticsearch cluster.

* Load batch & aggregated features from elasticache redis cluster.

* Assembly batch features and real-time features and prepare the payload

* Call the sagemaker real-time endpoint with the prepared payload and return the recommendations.

### How to deploy the lambda? ###

* go to folder aiq_realtime_architecture/recommendation-api-serverless
* execute './deploy.sh {env}'
* env could be dev,qa,prod etc.
