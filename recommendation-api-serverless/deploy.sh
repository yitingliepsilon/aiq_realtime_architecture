#!/bin/bash
python3 ./generate_env.py
cd recommendation-function
serverless deploy  --force --stage $1
rm ../env.json