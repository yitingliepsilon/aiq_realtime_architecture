import json
import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import random
#logging
import logging
logger = logging.getLogger(__name__)
class ELK():
    def __init__(self, es_endpoint = None, es_username = None, es_password = None):
        self.es_endpoint = es_endpoint
        self.es_username = es_username
        self.es_password = es_password
        
    def getJson(self,
                ref_time = None,
                lookBackInterval = None,
                parentID = None,
                customerID = None,
                es_index = None,
                es_header = None
                ):
        cutoff_time = ref_time - int(lookBackInterval[:-1])*3600*1000
        query = {
  "_source":[
    "message"
  ],
  "query":{
    "bool":{
      "filter":[
        {
          "term":{
            "log_group":"/aws/lambda/KinesisEventHandler"
          }
        },
        {
          "term":{
            "parentId": parentID
          }
        },
        {
          "term":{
            "loggerName":"com.epsilon.agilityevents.handler.BaseEventHandler"
          }
        },
        {
          "term":{
            "level":"INFO"
          }
        },
        {
          "term":{
              "epsilonCustomerId":customerID
          }   
        },
        {
          "range":{
            "@timestamp":{
              "gte":cutoff_time,
              "lte":ref_time,
            }
          }
        }
      ]
    }
  }
}
        random.shuffle(self.es_endpoint)
        auth = HTTPBasicAuth(self.es_username, self.es_password)
        #this can be furthere optimized if there are more than 2 endpoints available in the future. 
        while(self.es_endpoint):
            url = 'https://' + self.es_endpoint.pop() + '/' + es_index + '/_search?'
            response = requests.post(url, headers = es_header,auth = auth,data = json.dumps(query))  
            if response.status_code == requests.codes.ok:
                result = json.loads(response.text) 
                return result
        #todo log
        response.raise_for_status()
        return None
    
    def flattenJson(self,es_json = None, event_codes_dict = None):
        #todo
        #flatten and prepare a pandas dataframe from the result of getJson()
        json_all = []
        if (not "hits" in es_json) or (not es_json['hits']['hits']):
            #todo log
            logger.info("no record found in elasticsearch cluster, filling features with 0!")
            return pd.DataFrame(columns=['eventTimestamp','objectId','objectType','event_CD'])
        for i in es_json['hits']['hits']:
            json_current = json.loads(i['_source']['message'][16:])
            json_all.append(json_current)
        df_raw  = pd.DataFrame(json_all)
        df_raw['product'] = [i['event']['data']['products'] for i in df_raw['event']]
        #productionserver has Timestamp not eventTimestamp
        df_raw = df_raw.rename(columns = {'eventTimeStamp':'eventTimestamp'})
        df_raw = df_raw[['eventName','product','eventTimestamp']]
        df_raw = df_raw.explode('product')
        df_raw['objectId'] = [i['productId'] for i in df_raw['product']]
        df_raw['objectType'] = 'SKU'
        df_raw['event_CD'] = [event_codes_dict[i] for i in df_raw['eventName']]
        del df_raw['product']
        del df_raw['eventName']
        logger.debug("raw dataframe from elk: {}".format(df_raw.to_string()))
        return df_raw