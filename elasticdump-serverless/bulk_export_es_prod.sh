#!/bin/sh
today=$(date +'%Y-%m-%d')
 

elasticdump \
--s3Region="us-east-1" \
--input='https://agilityevent_prod_rpt_user:elk$prd&evnts!@logingest-east-1b.epsilonagilityloyalty.com:443/functionbeat-agilityevent*/' \
--output=s3://test-yiting/BJ/prod_${today}.json \
--type=data \
--searchBody='{"_source":["message"],"query":{"bool":{"filter":[{"term":{"log_group":"/aws/lambda/KinesisEventHandler"}},{"term":{"parentId": "60494"}},{"term":{"loggerName":"com.epsilon.agilityevents.handler.BaseEventHandler"}},{"term":{"level":"INFO"}},{
"range":{"@timestamp":{"gte":1587767100000,"lte":1587767183925,"format":"epoch_millis"}}}]}}}' \
--limit=10000 \
--concurrency=3 \
--concurrencyInterval=500 \
--sourceOnly=true \
--noRefresh=true \
--support-big-int=true