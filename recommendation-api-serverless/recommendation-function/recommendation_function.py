#!/usr/bin/env python
# coding: utf-8

#import python classes
import time
import numpy as np
import redis
import rediscluster
from rediscluster import StrictRedisCluster
import boto3
import ast
import json
import os
import sys

#import custom classes
from ELK import ELK
from EC import EC
from HourlyIVCreationRealTime import HourlyIVCreationRealTime
from SM import SM

#logging
import logging
logger_root = logging.getLogger()
'''
if logger_root.handlers:
    for handler in logger_root.handlers:
        logger_root.removeHandler(handler)
root_formatter = logging.Formatter('[%(levelname)s]\t%(asctime)s.%(msecs)dZ\t%(message)s\n') 
myRootHandler = logging.StreamHandler()
myRootHandler.setFormatter(root_formatter)
logger_root.addHandler(myRootHandler)
'''
logger_root.setLevel(logging.DEBUG)

# Simple exception wrappers
class ClientException(Exception):
    pass

class LambdaException(Exception):
    pass

env = os.environ['env']
redis_endpoint = os.environ['ec']
redis_timeout = int(os.environ['ec_timeout'])
redis_connection_timeout = int(os.environ['ec_con_timeout'])
metadata_time = time.time() 

#connect to redis
#todo test reds_endpoint is None
#need to test what if redis_endpoint connection  can't be established.
try:
    redis_host , redis_port = redis_endpoint.split(':')
    redis_startup_nodes = [{"host":
                        redis_host, "port": redis_port}]
    #the reason why it set to 1s timeout is there are 3 more retries.so 1s will be 4s at runtime, there is a bug on parameter retry_on_timeout (not working when set to False) in redis-py library , later version of redis-py fixed this bug, but redos-py-cluster can not support that version of redis-py.So we will leave 1s here and we can update it once redis-py-cluster support later version of redis-py.
    redis = StrictRedisCluster(startup_nodes=redis_startup_nodes,
                               decode_responses=True,
                               skip_full_coverage_check=True,
                               socket_timeout=redis_timeout,
                               socket_connect_timeout=redis_connection_timeout,
                               retry_on_timeout=False
                              )
except rediscluster.exceptions.RedisClusterException as e: 
    logger_root.error(e, exc_info=True)
    raise
except Exception as e:
    logger_root.error(e, exc_info=True)
    raise
    
#    raise redis.ConnectionError()
#except redis.ConnectionError as e:
#    logger.error("redis connection error", exc_info=True)
#    raise
#except Exception as e:
#    logger.error(e, exc_info=True)
#    raise

redis_connection_time = time.time()
logger_root.debug('elasticache connection time spent:' + str(redis_connection_time - metadata_time) + 's')

boto3_connection_time = time.time()
runtime_client = boto3.client('runtime.sagemaker')
logger_root.debug('boto3 connection time spent:' + str(boto3_connection_time - redis_connection_time) + 's')

def main(event,context):
    start_time = time.time()
    print("start ok!")
    event_keys = event.keys()
    
    #setup correlationId logging
    try:
        if 'correlationId' not in event_keys:
            correlationId = context.aws_request_id
        else:
            correlationId = event['correlationId']
        try:
            extra = {'correlationId': correlationId, 'requestID': context.aws_request_id}
            if not isinstance(correlationId, str):
                msg = "[correlationId] not string"
                raise ClientException(msg)
        except ClientException as e:
            logger_root.error(e, exc_info=True)
            return {"error": 400 , "message": msg}
        else:
            logger = logger_root.getChild(__name__)      
            myHandler = logging.StreamHandler()
            formatter = logging.Formatter('[%(levelname)s]\t%(asctime)s.%(msecs)dZ\t%(correlationId)s\t%(requestID)s\t%(message)s\n')
            myHandler.setFormatter(formatter)
            logger.addHandler(myHandler)
            logger = logging.LoggerAdapter(logger, extra)
            logger.propagate = False
    except Exception as e:
        logger_root .error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error setting up correlationId logging, check log for details"}
        
    #get event parameters
    try:         
        try:
            if 'tenant' not in event_keys:
                msg = "missing field [tenant]"
                raise ClientException(msg)
        except ClientException as e:          
            logger.error(e, exc_info=True)
            return {"error": 400 , "message": msg}
        else:
            tenantID = event['tenant']
            try:
                if not isinstance(tenantID, str):
                    msg = "[tenant] not string"
                    raise ClientException(msg)
            except ClientException as e:
                logger.error(e, exc_info=True)
                return {"error": 400 , "message": msg}
        try:
            if 'model' not in event_keys:
                msg = "missing field [model]"
                raise ClientException(msg)
        except ClientException as e:          
            logger.error(e, exc_info=True)
            return {"error": 400 , "message": msg}
        else:
            try:
                modelID = event['model']
                if not isinstance(modelID, str):
                    msg = "[model] not string"
                    raise ClientException(msg)
            except ClientException as e:
                logger.error(e, exc_info=True)
                return {"error": 400 , "message": msg}
        if 'modelrun' not in event_keys:
            modelRunID = 'v1'
        else:
            try:
                modelRunID = event['modelrun']
                if not isinstance(modelRunID, str):                    
                    msg = "[modelrun] not string"
                    raise ClientException(msg)
            except ClientException as e:
                logger.error(e, exc_info=True)
                return {"error": 400 , "message": msg}
        try:
            if 'customer' not in event_keys:
                msg = "missing field [customer]"
                raise ClientException(msg)
        except ClientException as e:          
            logger.error(e, exc_info=True)
            return {"error": 400 , "message": msg}
        else:
            try:
                customerID = event['customer']
                if not isinstance(customerID, str):
                    msg = "[customer] not string"
                    raise ClientException(msg)
            except ClientException:
                logger.error(e, exc_info=True)
                return {"error": 400 , "message": msg}
        if 'numofchoice' not in event_keys:
            num_of_choice = -1
        else:
            try:
                num_of_choice = event['numofchoice']
                if not (isinstance(num_of_choice, int)):
                    msg = "[numofchoice] not string"
                    raise ClientException(msg)
            except ClientException:
                logger.error(e, exc_info=True)
                return {"error": 400 , "message": msg}

        init_time = time.time()
        logger.debug('get events parameters time spent total:' + str(init_time-start_time) + 's')
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error getting event parameters,check log for details"}
    ref_time = int(round(time.time() * 1000))
    
    try:
        try:
            # get tenant-settings from elasticache
            tenantSettings = EC.getTenantSettings(redis = redis, tenantID = tenantID, env = env)
            if not tenantSettings:
                msg = "tenantSettings not found in elasticache"
                raise LambdaException(msg)
        except LambdaException:
            logger.error(e, exc_info=True)
            return {"error": 404, "message": msg}
        #get tenant-settings variables
        es_username = tenantSettings["integrations"]["events"]["elkUsernameSecretString"]
        es_password = tenantSettings["integrations"]["events"]["elkPasswordSecretString"]  
        es_endpoint = tenantSettings["integrations"]["events"]['elkEndpoints'].split(',')
        es_header = {"Content-Type": "application/json"}
        es_index = tenantSettings["integrations"]["events"]['elkIndex']
        parentID = tenantSettings["integrations"]["events"]['parentId']
        defaultApplication = tenantSettings['defaultApplication']
        if 'application' not in event_keys:
            applicationID = defaultApplication
        else:
            try:
                applicationID = event['application']
                if not isinstance(applicationID, str):
                    msg = "[application] not string"
                    raise ClientException(msg)
            except ClientException:
                logger.error(e, exc_info=True)
                return {"error": 400 , "message": msg}
        #get model-settings from elasticache
        try:
            modelSettings = EC.getModelSettings(redis, tenantID, applicationID, modelID, modelRunID)
            if not modelSettings:
                msg = "modelSettings not found in elasticache"
                raise LambdaException(msg)
        except LambdaException as e:            
            logger.error(e, exc_info=True)
            return {"error": 404, "message": msg}
        #get model-settings variables
        event_codes_dict = modelSettings['eventCodesDict']
        lookBackInterval = modelSettings['lookBackInterval']
        sagemakerEndpoint = modelSettings['sagemakerEndpoint']

        param_time = time.time()
        logger.debug('elasticsearch time spent:' + str(param_time-init_time) + 's')
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error getting tenant settings and model settings from elasticache, check log for details"}   
    #put batch features ahead of es to save  0.3s
    try:
        #get batch_feature names and values, get orderiv,get mapping table etc.
        batch_features = EC.getBatchIVs(redis = redis,
                                        tenantID = tenantID,
                                        applicationID = applicationID,
                                        modelID = modelID,
                                        modelRunID = modelRunID,
                                        customerID = customerID
                                       )
        try:
            if not batch_features:
                msg = "batch features not found in elasticache"
                raise LambdaException(msg)
        except LambdaException as e:            
            logger.error(e, exc_info=True)
            return {"error": 404, "message": msg}
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error getting batch features"}   
    
    #initiate ELK_instance, pull json and transfer to pandas dataframe
    #todo better error handling with known errors
    try:
        ELK_instance = ELK(es_endpoint = es_endpoint,
                           es_username = es_username, 
                           es_password = es_password)    
        df_json = ELK_instance.getJson(ref_time = ref_time,
                                       lookBackInterval = lookBackInterval,
                                       parentID = parentID,
                                       customerID = customerID,
                                       es_index = es_index,
                                       es_header = es_header
                                      )
        logger.info('df_json: {}'.format(df_json))

        df_raw = ELK_instance.flattenJson(es_json = df_json,
                                          event_codes_dict = event_codes_dict
                                         )
        es_time = time.time()
        logger.debug('elasticsearch time spent:' + str(es_time-param_time) + 's')
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error in module [ELK],check log for details"}       
    try:
        #need to make sure mapping_table is in ec
        df_mapped = EC.getMapping(redis = redis,
                                  df_mapped = df_raw,
                                  tenantID = tenantID,
                                  applicationID = applicationID,
                                  modelID = modelID,
                                  modelRunID = modelRunID
                                  )
        logger.info('dataframe after mapped - {}'.format(df_mapped.to_string()))

        OrderIV = EC.getOrderIVInfo(redis = redis,
                                    tenantID = tenantID,
                                    applicationID = applicationID,
                                    modelID = modelID,
                                    modelRunID = modelRunID
                                   )
        try:
            if not OrderIV:
                msg = "OrderIV not found in elasticache"
                raise LambdaException(msg)
        except LambdaException as e:            
            logger.error(e, exc_info=True)
            return {"error": 404, "message": msg}

        OrderIV_batch = EC.getOrderIVBatch(redis = redis,
                                           tenantID = tenantID,
                                           applicationID = applicationID,
                                           modelID = modelID,
                                           modelRunID = modelRunID
                                           )
        #todo retrieve image mapping
        
        try:
            if not OrderIV_batch:
                msg = "OrderIV_batch not found in elasticache"
                raise LambdaException(msg)
        except LambdaException as e:            
            logger.error(e, exc_info=True)
            return {"error": 404, "message": msg}

        #fix this in the future
        df_mapped['eventTimestamp'] = df_mapped['eventTimestamp'].astype(int)
        ec_time = time.time()
        logger.debug('elasticache time spent:' + str(ec_time-es_time) + 's')
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error in module [EC],check log for details"}   
               
    try:    
        #iv creation
        HIV = HourlyIVCreationRealTime(ref_time = ref_time)
        realtime_features = HIV.transform(OrderIV = list(set(OrderIV)-set(OrderIV_batch)),
                                          df = df_mapped)
        hiv_time = time.time()
        logger.debug('iv creation time spent:' + str(hiv_time-ec_time) + 's')
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error in module [HIV],check log for details"}
    
    try:
        #prepare payload,call sagemaker, and post process features
        payload = SM.preparePayLoad(OrderIV = OrderIV,
                                    OrderIV_batch = OrderIV_batch,
                                    batch_features = batch_features,
                                    realtime_features = realtime_features)
        
        result = SM.getPrediction(payload = payload,
                                  runtime_client = runtime_client,
                                  sagemakerEndpoint = sagemakerEndpoint)
        recommendations = SM.standardizeFormat(prediction_string = result,num_of_choice = num_of_choice)
        gp_time = time.time()
        logger.debug('get prediction time spent:' + str(gp_time-hiv_time) + 's')
    except Exception as e:
        logger.error(e, exc_info=True)
        return {"error": 500 , "message": "internal unhandled error in module [SM],check log for details"}
    logger.debug('total time spent:' + str(gp_time-start_time) + 's')
    logger.debug(recommendations)
    return recommendations
