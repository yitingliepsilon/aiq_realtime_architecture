#!/usr/bin/env python
# coding: utf-8

# this script is being used to generate env.json file

# In[6]:


import json
import boto3


# In[13]:


def create_json_file(my_dict):
    with open('env.json', 'w') as fp:
        json.dump(my_dict, fp)       
'''
my_dict = {}
my_dict["es_username"] = "agilityevent_prod_rpt_user"
my_dict["es_password"] =  response['SecretString']
my_dict["es_endpoint"] =  "logingest-east-1a.epsilonagilityloyalty.com:443"
my_dict["es_index"] =  "functionbeat-agilityevent*"
my_dict["es_BU"] =  "60494"
my_dict["s3_region"] = "us-east-1"
my_dict["output_path"] = "s3://test-yiting/BJ/"
my_dict['pem_bucket'] = "test-yiting"
my_dict['pem_path'] = "ec2-key/realtime-etl.pem"
my_dict['ec2_endpoint'] = 'ec2-184-73-105-194.compute-1.amazonaws.com'
my_dict['trigger_path'] = 'BJ-trigger/'
my_dict['trigger_suffix'] = '.csv'
my_dict['trigger_type'] = 's3:ObjectCreated:*'
my_dict['trigger_bucket'] = "test-yiting"
'''
   
client = boto3.client('s3')
bucket = 'iq-data-dev'
key = '__CONFIG__/dev-elasticdump.json'
elasticDumpSettings = client.get_object(Bucket = bucket ,Key = key)
elasticDumpSettings = elasticDumpSettings['Body'].read().decode('utf-8')
elasticDumpSettings = json.loads(elasticDumpSettings)
create_json_file(elasticDumpSettings)





