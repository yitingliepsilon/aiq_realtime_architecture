#!/usr/bin/env python
# coding: utf-8
# this script is being used to generate env.json file

import json
import boto3

client = boto3.client('s3')

def writeJsonFile(pythonDict):
    with open('env.json', 'w') as fp:
        json.dump(pythonDict, fp)
        
#download iq env config:
bucket = 'iq-data-dev'
key = '__CONFIG__/dev-settings.json'
iqConfig = client.get_object(Bucket = bucket ,Key = key)
iqConfig = iqConfig['Body'].read().decode('utf-8')
iqConfig = json.loads(iqConfig)
writeJsonFile(iqConfig)