#!/bin/bash
python3 ./generate_env.py
cd recommendation-function
serverless deploy  --force
rm ../env.json
